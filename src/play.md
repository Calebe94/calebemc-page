<script>
$("#index").addClass('active');
$("#rules").removeClass('active');
$("#staff").removeClass('active');
$("#mods").removeClass('active');
$("#maps").removeClass('active');
</script>
<div class="container">
# Jogar

Para jogar no CalebeMC você deverá primeiro enviar uma requisição para o administrador.
Você poderá fazer a solicitação através do formulário abaixo:

<form action="mailto:calebe94@disroot.org" method="post" enctype="text/plain">
Nome de usuário:<br><input type="text" name="username"> <br>
<input type="submit" value="Enviar">
</form>

Ou entrem em contato comigo pelo [Telegram](https://t.me/calebe94).

---

## Adicionando o servidor

Após se cadastrar, você deverá adicionar o servidor no seu Client (PC, Vídeo Game ou Smartphone).

### PC (Windows, MacOS e Linux)

Na tela inicial do seu Minecraft, clique em **Multiplayer** e em sequência clique em **Add Server**.

Em seguida preencha o formulário com as seguintes informações:

* **Server Name**: `CalebeMC`;
* **Server Address**: `minecraft.calebe.dev.br`.

<img src="assets/img/tutorial/server_info.jpg" width="400px">

### Smartphone (Android e iOS)

Em breve ...

### Nintendo Switch

Em breve ...

</div>
