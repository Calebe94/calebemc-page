<script>
$("#index").addClass('active');
$("#rules").removeClass('active');
$("#staff").removeClass('active');
$("#mods").removeClass('active');
$("#maps").removeClass('active');
</script>
<div class="jumbotron homepage-opening">
<div class="container">
<div class="row text-center">
<h1 class="head">CalebeMC</h1>
<h2 class="leader">minecraft.calebe.dev.br</h2>
<br />
<center><a class="btn btn-huge btn-info btn-glow" href="play.html">Jogar</a></center>
</div>
</div>
</div>
<!-- /Big opening -->
<div class="container">
<div class="row text-center mc-items">
<div class="col-md-4">
<div class="icon">
<img src="assets/img/swords.png" />
</div>
<h2>Batalhe contra monstros</h2>
<p>Destrua monstros para coletar os seus drops.
</p>
</div>
<div class="col-md-4">
<div class="icon">
<img src="assets/img/chest.png" />
</div>
<h2>Colete materiais</h2>
<p>Colete materiais do Overworld, Nether e The End.
</p>
</div>
<div class="col-md-4">
<div class="icon">
<img src="assets/img/workbench.png" />
</div>
<h2>Construa</h2>
<p>Seja criativo. Contrua oque vier a sua mente!
</p>
</div>
</div>
</div>
