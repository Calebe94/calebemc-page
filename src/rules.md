<script>
$("#index").removeClass('active');
$("#rules").addClass('active');
$("#staff").removeClass('active');
$("#mods").removeClass('active');
$("#maps").removeClass('active');
</script>
<div class="container">

<h1 class="page-header">Regras</h1>

Siga o conselho do Bill and Ted, e "sejam excelentes ums com os outros"!

![Bill and Ted: be excellent to each other](assets/img/be_excellent_to_each_other.jpg)

## Chat

* Não spammar;
* Não insultar ninguém;

## Gameplay

* Não matar o companheirinho;
* Não destruir as construções dos outros;
* Não utilizar hacks;
</div>
