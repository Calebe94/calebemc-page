
ssg5_path=ssg5
title="CalebeMC"
domain="https://minecraft.calebe.dev.br/"
src_folder=src/
dst_folder=site/

folder=/var/www/my_webapp/www/
ssh=test@test.local

${dst_folder}:
	@echo "Creating dst directory"
	mkdir -p ${dst_folder}

all: deploy

prebuild:
	@echo "prebuild"
	./generate_index.sh

postbuild:
	@echo "postbuild"

debug: build_dbg
	python -m http.server

build_dbg: ${dst_folder} prebuild
	#sed -i 's+http://calebe.ml/site+http://localhost:8000/site+g' src/_header.html
	${ssg5_path} ${src_folder} ${dst_folder} ${title} http://0.0.0.0:8000
	cp -r assets/ font-awesome/ ${dst_folder}

build: ${dst_folder} prebuild
	#sed -i 's+http://localhost:8000/site+http://calebe.ml/site+g' src/_header.html
	${ssg5_path} ${src_folder} ${dst_folder} "${title}" "${domain}"
	cp -r assets/ font-awesome/ ${dst_folder}

deploy: build
	scp -r ${dst_folder}/* ${ssh}:${folder}

.PHONY: all build deploy debug build_dbg prebuild postbuild
