#!/bin/bash

generate_index()
{
  path="$1"
  for md_file in $path/*.md; do
    if [ -f $md_file ] && [ "$(basename $md_file)" != "index.md" ]; then
      title=$(tyaml $md_file -v title)
      date=$(tyaml $md_file -v date)
      echo "* [$title]($(basename ${md_file%.*}.html))" >> $path/aux_index.txt
    fi
  done
}

find "src/" -type d \
  -not -path "src/" \
  -not -path "src/static" \
  -not -path "src/static/*" \
  -not -path "src/blog/media" \
  -print0 | while IFS= read -r -d '' file; do
    printf '%s\n' "$file"
    cat $file/index.txt > $file/aux_index.txt
    generate_index $file
    mv $file/aux_index.txt $file/index.md
  done

